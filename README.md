Role Name
=========

Setup tools install system deimos-car

Requirements
------------

- Ansible > 2.6
- git
- network ready (wifi or lane)

Role Variables
--------------
- Install Base packages bootstrap
- Install ROS Kinetic core config_system
- Configure services samba and avahi
- Update firmware and driver cars
- roslaunch node startup cars

Dependencies
------------

ROS Kinetic ONLY supports Wily (Ubuntu 15.10), Xenial (Ubuntu 16.04) and Jessie (Debian 8) for debian packages.

Install Ansible to ubuntu mate
------------

Step 1 — Installing Ansible
To begin exploring Ansible as a means of managing our various servers, we need to install the Ansible software on at least one machine. We will be using an Ubuntu 16.04 server for this section.

The best way to get Ansible for Ubuntu is to add the project's PPA (personal package archive) to your system. We can add the Ansible PPA by typing the following command:

```bash
sudo apt-add-repository ppa:ansible/ansible
Press ENTER to accept the PPA addition.
```

Next, we need to refresh our system's package index so that it is aware of the packages available in the PPA. Afterwards, we can install the software:

```bash
sudo apt-get update
sudo apt-get install ansible
```
As we mentioned above, Ansible primarily communicates with client computers through SSH. While it certainly has the ability to handle password-based SSH authentication, SSH keys help keep things simple. You can follow the tutorial linked in the prerequisites to set up SSH keys if you haven't already.

We now have all of the software required to administer our servers through Ansible.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: localhost
      roles:
         - deimos-car

Author Information
------------------

TEAM Deimos RobotCar
