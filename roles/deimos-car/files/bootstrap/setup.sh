#!/bin/bash

if ping -q -c 1 -W 1 8.8.8.8 >/dev/null; then
  echo "IPv4 is up -> ADD GPG REPO ANSIBLE"

  apt-add-repository -y ppa:ansible/ansible
  apt-get update
  apt-get -y install ansible git htop vim
  apt-get -y install --only-upgrade ansible

  echo "IPv4 is up -> Install Ros Env Car"
  ansible-pull -U https://gitlab.com/ros-deimos/car-setup/deimos-install.git -d /etc/ansible/playbooks/deimos-install/

else
  echo "IPv4 is down -> Bootstrap no load run at next reboot"
fi
